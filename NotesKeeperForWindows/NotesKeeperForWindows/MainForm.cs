﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesKeeperForWindows
{
    public partial class MainForm : Form
    {
        int categoryId = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadCategories();
        }

        private void CopyMenu_Click(object sender, EventArgs e)
        {
            richText.Copy();
        }

        private void CutMenu_Click(object sender, EventArgs e)
        {
            richText.Cut();
        }

        private void PastMenu_Click(object sender, EventArgs e)
        {
            richText.Paste();
        }

        private void ExitMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ShowButton_Click(object sender, EventArgs e)
        {
            if (categoryList.SelectedItem != null){
                categoryId = GetCategoryIdByName();
                noteList.Items.Clear();
                LoadNotes(categoryId);
            }
            else {
                statusLabel.Text = "";
                statusLabel.Text = "Category is not selected.";
            }
            
        }

        private void ReadButton_Click(object sender, EventArgs e)
        {
            if (noteList.SelectedItem != null){
                richText.Clear();
                titleText.Clear();
                titleText.Text = (String)noteList.SelectedItem;
                ShowNote();
            }
            else{
                statusLabel.Text = "";
                statusLabel.Text = "Note is not selected.";
            }
        }

        private void NewCategoryMenu_Click(object sender, EventArgs e)
        {
            NewCategory nc = new NewCategory();
            nc.ShowDialog();
            if (nc.DialogResult == DialogResult.OK)
            {
                categoryList.Items.Clear();
                LoadCategories();
            }
        }

        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (categoryList.SelectedItem != null)
            {
                int id = GetCategoryIdByName();
                RemoveCategory(id);
                int index = categoryList.SelectedIndex;
                categoryList.Items.RemoveAt(index);
            }
            else {
                statusLabel.Text = "";
                statusLabel.Text = "Category is not selected.";
            }
        }

        private void NewNoteMenu_Click(object sender, EventArgs e)
        {
            if (categoryList.SelectedItem != null)
            {
                titleText.Text = "";
                richText.Text = "";
                noteList.ClearSelected();
            }
        }

        private void AddNoteButton_Click(object sender, EventArgs e)
        {
            string title = titleText.Text;
            string description = richText.Text;
            int id = GetCategoryIdByName();
            AddNote(title, description);
            noteList.Items.Add(title);
        }

        private void SaveNoteMenu_Click(object sender, EventArgs e)
        {
            string title = titleText.Text;
            string description = richText.Text;
            UpdateNote(title, description);
            noteList.Items.Clear();
            LoadNotes(categoryId);
        }
        
        private void DeleteNoteMenu_Click(object sender, EventArgs e)
        {
            int id = GetNoteIdByName();
            int index = noteList.SelectedIndex;
            DeleteNote(id);
            noteList.Items.RemoveAt(index);
            richText.Clear();
            titleText.Clear();
        }
        
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

        private void LoadCategories()
        {
            SqlCommand command;
            SqlConnection connection = Program.GetConnection();
            command = new SqlCommand("SELECT COUNT(*) FROM Category", connection);
            int count = (int)command.ExecuteScalar();
            command = new SqlCommand("SELECT * FROM Category", connection);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                for (int i = 0; i < count; i++)
                {
                    reader.Read();
                    categoryList.Items.Add(reader.GetString(1));
                }
            }
        }

        private void LoadNotes(int categoryId)
        {
            SqlCommand command;
            SqlConnection connection = Program.GetConnection();
            command = new SqlCommand("SELECT COUNT(*) FROM Notes WHERE CategoryId = @Id", connection);
            command.Parameters.Add("@Id", SqlDbType.Int).Value = categoryId;
            int count = (int)command.ExecuteScalar();
            command = new SqlCommand("SELECT * FROM Notes WHERE CategoryId = @Id", connection);
            command.Parameters.Add("@Id", SqlDbType.Int).Value = categoryId;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                for (int i = 0; i < count; i++)
                {
                    reader.Read();
                    noteList.Items.Add(reader.GetString(1));
                }
            }
        }

        private void ShowNote()
        {
            SqlCommand command;
            SqlConnection connection = Program.GetConnection();
            string name = (string)noteList.SelectedItem;
            command = new SqlCommand("SELECT * FROM Notes WHERE Title=@name", connection);
            command.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                    reader.Read();
                    richText.Text = reader.GetString(2);
            }
        }

        private void RemoveCategory(int id)
        {
            SqlConnection connection = Program.GetConnection();
            SqlCommand command1 = new SqlCommand("DELETE FROM Notes WHERE CategoryId = @Id", connection);
            command1.Parameters.Add("@id", SqlDbType.Int).Value = id;
            command1.ExecuteNonQuery();
            SqlCommand command = new SqlCommand("DELETE FROM Category WHERE Id = @id", connection);
            command.Parameters.Add("@id", SqlDbType.Int).Value = id;
            command.ExecuteNonQuery();
        }

        private void AddNote(string title, string description)
        {
            SqlConnection connection = Program.GetConnection();
            int forKey = GetCategoryIdByName();
            SqlCommand command = new SqlCommand("INSERT INTO Notes(Title, Description, CategoryId) Values (@title, @description, @forKey)", connection);
            command.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
            command.Parameters.Add("@description", SqlDbType.VarChar).Value = description;
            command.Parameters.Add("@forKey", SqlDbType.Int).Value = forKey;
            command.ExecuteNonQuery();
        }

        private void UpdateNote(string title, string description) {
            SqlConnection connection = Program.GetConnection();
            int forKey = GetCategoryIdByName();
            SqlCommand command = new SqlCommand("UPDATE Notes SET Title=@title, Description=@description WHERE Title=@title", connection);
            command.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
            command.Parameters.Add("@description", SqlDbType.VarChar).Value = description;
            command.ExecuteNonQuery();
        }

        private void DeleteNote(int id)
        {
            SqlConnection connection = Program.GetConnection();
            SqlCommand command = new SqlCommand("DELETE FROM Notes WHERE CategoryId = @Id", connection);
            command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            command.ExecuteNonQuery();
        }

        private int GetCategoryIdByName()
        {
            SqlCommand command;
            SqlConnection connection = Program.GetConnection();
            string name = (string)categoryList.SelectedItem;
            int id;
            command = new SqlCommand("SELECT Id FROM Category WHERE Name=@name", connection);
            command.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                id = reader.GetInt32(0);
            }
            return id;
        }

        private int GetNoteIdByName()
        {
            SqlCommand command;
            SqlConnection connection = Program.GetConnection();
            string name = (string)noteList.SelectedItem;
            int id;
            command = new SqlCommand("SELECT Id FROM Notes WHERE Title=@name", connection);
            command.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                id = reader.GetInt32(0);
            }
            return id;
        }
    }
}
