﻿namespace NotesKeeperForWindows
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newCategoryMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCategoryMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.newNoteMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveNoteMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteNoteMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.copyMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.cutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.pastMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.showButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.categoryList = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.addNoteButton = new System.Windows.Forms.Button();
            this.titleText = new System.Windows.Forms.TextBox();
            this.richText = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.noteList = new System.Windows.Forms.ListBox();
            this.readButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(646, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCategoryMenu,
            this.deleteCategoryMenu,
            this.toolStripSeparator1,
            this.newNoteMenu,
            this.saveNoteMenu,
            this.deleteNoteMenu,
            this.toolStripSeparator2,
            this.exitMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "File";
            // 
            // newCategoryMenu
            // 
            this.newCategoryMenu.Name = "newCategoryMenu";
            this.newCategoryMenu.Size = new System.Drawing.Size(158, 22);
            this.newCategoryMenu.Text = "New Category";
            this.newCategoryMenu.Click += new System.EventHandler(this.NewCategoryMenu_Click);
            // 
            // deleteCategoryMenu
            // 
            this.deleteCategoryMenu.Name = "deleteCategoryMenu";
            this.deleteCategoryMenu.Size = new System.Drawing.Size(158, 22);
            this.deleteCategoryMenu.Text = "Delete Category";
            this.deleteCategoryMenu.Click += new System.EventHandler(this.ToolStripMenuItem3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(155, 6);
            // 
            // newNoteMenu
            // 
            this.newNoteMenu.Name = "newNoteMenu";
            this.newNoteMenu.Size = new System.Drawing.Size(158, 22);
            this.newNoteMenu.Text = "New Note";
            this.newNoteMenu.Click += new System.EventHandler(this.NewNoteMenu_Click);
            // 
            // saveNoteMenu
            // 
            this.saveNoteMenu.Name = "saveNoteMenu";
            this.saveNoteMenu.Size = new System.Drawing.Size(158, 22);
            this.saveNoteMenu.Text = "Save Note";
            this.saveNoteMenu.Click += new System.EventHandler(this.SaveNoteMenu_Click);
            // 
            // deleteNoteMenu
            // 
            this.deleteNoteMenu.Name = "deleteNoteMenu";
            this.deleteNoteMenu.Size = new System.Drawing.Size(158, 22);
            this.deleteNoteMenu.Text = "Delete Note";
            this.deleteNoteMenu.Click += new System.EventHandler(this.DeleteNoteMenu_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(155, 6);
            // 
            // exitMenu
            // 
            this.exitMenu.Name = "exitMenu";
            this.exitMenu.Size = new System.Drawing.Size(158, 22);
            this.exitMenu.Text = "Exit";
            this.exitMenu.Click += new System.EventHandler(this.ExitMenu_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyMenu,
            this.cutMenu,
            this.pastMenu});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(39, 20);
            this.editMenu.Text = "Edit";
            // 
            // copyMenu
            // 
            this.copyMenu.Name = "copyMenu";
            this.copyMenu.Size = new System.Drawing.Size(102, 22);
            this.copyMenu.Text = "Copy";
            this.copyMenu.Click += new System.EventHandler(this.CopyMenu_Click);
            // 
            // cutMenu
            // 
            this.cutMenu.Name = "cutMenu";
            this.cutMenu.Size = new System.Drawing.Size(102, 22);
            this.cutMenu.Text = "Cut";
            this.cutMenu.Click += new System.EventHandler(this.CutMenu_Click);
            // 
            // pastMenu
            // 
            this.pastMenu.Name = "pastMenu";
            this.pastMenu.Size = new System.Drawing.Size(102, 22);
            this.pastMenu.Text = "Paste";
            this.pastMenu.Click += new System.EventHandler(this.PastMenu_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 379);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(646, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.showButton);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.categoryList);
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 349);
            this.panel1.TabIndex = 2;
            // 
            // showButton
            // 
            this.showButton.Location = new System.Drawing.Point(12, 318);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(90, 28);
            this.showButton.TabIndex = 5;
            this.showButton.Text = "Show";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.ShowButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft MHei", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Categories";
            // 
            // categoryList
            // 
            this.categoryList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.categoryList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.categoryList.Font = new System.Drawing.Font("Microsoft MHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoryList.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.categoryList.FormattingEnabled = true;
            this.categoryList.ItemHeight = 26;
            this.categoryList.Location = new System.Drawing.Point(0, 37);
            this.categoryList.Name = "categoryList";
            this.categoryList.Size = new System.Drawing.Size(119, 286);
            this.categoryList.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.addNoteButton);
            this.panel2.Controls.Add(this.titleText);
            this.panel2.Controls.Add(this.richText);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(117, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(377, 348);
            this.panel2.TabIndex = 3;
            // 
            // addNoteButton
            // 
            this.addNoteButton.Location = new System.Drawing.Point(276, 9);
            this.addNoteButton.Name = "addNoteButton";
            this.addNoteButton.Size = new System.Drawing.Size(75, 23);
            this.addNoteButton.TabIndex = 5;
            this.addNoteButton.Text = "Add Note";
            this.addNoteButton.UseVisualStyleBackColor = true;
            this.addNoteButton.Click += new System.EventHandler(this.AddNoteButton_Click);
            // 
            // titleText
            // 
            this.titleText.Font = new System.Drawing.Font("Microsoft MHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleText.Location = new System.Drawing.Point(52, 8);
            this.titleText.Multiline = true;
            this.titleText.Name = "titleText";
            this.titleText.Size = new System.Drawing.Size(170, 25);
            this.titleText.TabIndex = 4;
            // 
            // richText
            // 
            this.richText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.richText.Location = new System.Drawing.Point(7, 36);
            this.richText.Name = "richText";
            this.richText.Size = new System.Drawing.Size(367, 309);
            this.richText.TabIndex = 3;
            this.richText.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft MHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "Title";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft MHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 28);
            this.label2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel3.Controls.Add(this.noteList);
            this.panel3.Controls.Add(this.readButton);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(500, 28);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(146, 348);
            this.panel3.TabIndex = 4;
            // 
            // noteList
            // 
            this.noteList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.noteList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.noteList.Font = new System.Drawing.Font("Microsoft MHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noteList.FormattingEnabled = true;
            this.noteList.ItemHeight = 26;
            this.noteList.Location = new System.Drawing.Point(3, 36);
            this.noteList.Name = "noteList";
            this.noteList.Size = new System.Drawing.Size(140, 260);
            this.noteList.TabIndex = 5;
            // 
            // readButton
            // 
            this.readButton.Location = new System.Drawing.Point(30, 317);
            this.readButton.Name = "readButton";
            this.readButton.Size = new System.Drawing.Size(90, 28);
            this.readButton.TabIndex = 4;
            this.readButton.Text = "Read";
            this.readButton.UseVisualStyleBackColor = true;
            this.readButton.Click += new System.EventHandler(this.ReadButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft MHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(-2, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 28);
            this.label4.TabIndex = 3;
            this.label4.Text = "Note List";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 401);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Notes Keeper";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newCategoryMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteCategoryMenu;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox categoryList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem newNoteMenu;
        private System.Windows.Forms.ToolStripMenuItem saveNoteMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteNoteMenu;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitMenu;
        private System.Windows.Forms.TextBox titleText;
        private System.Windows.Forms.RichTextBox richText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox noteList;
        private System.Windows.Forms.Button readButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem copyMenu;
        private System.Windows.Forms.ToolStripMenuItem cutMenu;
        private System.Windows.Forms.ToolStripMenuItem pastMenu;
        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.Button addNoteButton;
    }
}

