﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesKeeperForWindows
{
    static class Program
    {
        private static SqlConnection connection;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            String connectionString = @"Data Source=DESKTOP-PLUND4J;Initial Catalog=NotesKeeperDB;Integrated Security=True;Persist Security Info=False;";
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (connection = new SqlConnection(connectionString))
            {
                connection.Open();
                Application.Run(new MainForm());
            }

        }

        public static SqlConnection GetConnection()
        {
            return connection;
        }
    }
}
