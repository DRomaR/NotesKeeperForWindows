﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotesKeeperForWindows
{
    public partial class NewCategory : Form
    {
        public NewCategory()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            string name = newCategoryText.Text.Trim();
            AddCategory(name);
        }

        private void AddCategory(string name)
        {
            SqlConnection connection = Program.GetConnection();
            SqlCommand command = new SqlCommand("INSERT INTO Category(Name) Values (@Name)", connection);
            command.Parameters.Add("@Name", SqlDbType.VarChar).Value = name;
            command.ExecuteNonQuery();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
